<?php include_once('header.php'); ?>

<h2>Phone Carrier Help</h2>

If your carrier is not listed or not working, please contact me immediately and I will attempt to fix the problem.  If your carrier does not have an SMS gateway, there is nothing I can really do but I can definitely help you find out if your carrier does, and get it added to this script.

<br /><br />

Make sure you mention what carrier you use and your phone number if this is the issue.  I may send some test messages if your carrier exists, so be prepared for that.

<h2>Server Side Interface</h2>

If you have a problem with this interface, then all I need to know is your Pardus character as you've probably already created an account.  If you haven't, more information might be necessary but I doubt it.

<h2>Client Side Interface</h2>

If you have problems with the client side script, please mention your character and what browser you are using, as well as the version.  If you are using Firefox, then also mention what version of GreaseMonkey you have installed.  If you don't know how to get any of this information, just ask.

<h2>Other People/Abuse</h2>

If someone uses your character name, go ahead and sign up with your other characters and send me a message.  That user will be banned and I will allow you to fix your account or possibly do it myself.  I need proof, though, and you must contact me in the same universe within Pardus to prove that you are who you say you are.

<h2>Emails</h2>

I prefer PMs, but if you need to email me make sure you mention Pardus and what project this is.  Also mention what your character name(s) are and what universe(s) you play in.

<h2>Information</h2>

As is in the footer, here is my contact information.

<br /><br />

Orion: <?php echo Config::ADMIN_ORION; ?>
<br />
Artemis: <?php echo Config::ADMIN_ARTEMIS; ?>
<br />
Pegasus: <?php echo Config::ADMIN_PEGASUS; ?>

<br /><br />

Email Address:
<a href="mailto:<?php echo Config::ADMIN_EMAIL; ?>">
<?php echo Config::ADMIN_EMAIL; ?>
</a>

<br /><br />

<?php include_once('footer.php'); ?>
