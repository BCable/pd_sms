<?php
include_once('header.php');
include_once('classes/message.php');

$security->require_login();

if(isset($_POST['test'])){
	$msg = new Message();
	$msg->send(
		$security->acct,
		$_GET['character'],
		'Test Message',
		'This is a test message from the Pardus Combat SMS Relay.'
	);
	die_clean('Test message sent.');
}

?>

<h2>Test Message</h2>

Click the button below to send a test message to your phone.

<br /><br />

<form method="post">
	<input type="submit" name="test" value="Send Test Message" class="styled" />
</form>

<?php include_once('footer.php'); ?>
