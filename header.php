<?php
include_once('setup.php');

function is_page($page){
	if($page == ''){
		if(substr($_SERVER['PHP_SELF'],-10)=='/index.php')
			echo ' highlight';
	} else {
		if(substr($_SERVER['PHP_SELF'],-strlen($page)-1)=="/{$page}")
			echo ' highlight';
	}
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" 
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html>

<head>
	<title>Pardus Combat SMS Relay</title>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
	<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>

<div style="width: 750px; margin-left: auto; margin-right: auto">

<div style="float: left; clear: both; width: 100%">
	<h1>Pardus Combat SMS Relay</h1>
</div>

<div class="fleft">

<table cellspacing="0" cellpadding="0" class="menu">
	<tr class="title"><th>General</th></tr>
	<tr class="middle<?php echo is_page(''); ?>">
		<td><a href="index.php">Main</a></td>
	</tr>
	<tr class="middle<?php echo is_page('privacy.php'); ?>">
		<td><a href="privacy.php">Privacy Policy</a></td>
	</tr>
	<tr class="middle<?php echo is_page('contact.php'); ?>">
		<td><a href="contact.php">Help/Contact</a></td>
	</tr>
	<tr class="middle<?php echo is_page('carriers.php'); ?>">
		<td><a href="carriers.php">Carrier Info</a></td>
	</tr>
	<tr class="middle bottom">
		<td><a href="http://git.bcable.net/?p=pd_sms.git">Source Code</a></td>
	</tr>
</table>

<div class="clrb"><br /></div>

<table cellspacing="0" cellpadding="0" class="menu">
	<tr class="title"><th>User</th></tr>

<?php
	if(!$security->is_logged_in()){ ?>
	<tr class="middle<?php echo is_page('register.php'); ?>">
		<td><a href="register.php">Register</a></td>
	</tr>
	<tr class="middle<?php echo is_page('login.php'); ?> bottom">
		<td><a href="login.php">Login</a></td>
	</tr>
<?php
	}

	else { ?>
	<tr class="middle<?php echo is_page('edit_info.php'); ?>">
		<td><a href="edit_info.php">Edit Information</a></td>
	</tr>
	<tr class="middle">
		<td><a href="client/pd_sms.user.js">Install Script</a></td>
	</tr>
	<tr class="middle<?php echo is_page('test_message.php'); ?>">
		<td><a href="test_message.php">Test Message</a></td>
	</tr>
	<tr class="middle<?php echo is_page('logs.php'); ?>">
		<td><a href="logs.php">View Logs</a></td>
	</tr>
	<tr class="middle<?php echo is_page('logout.php'); ?> bottom">
		<td><a href="logout.php">Logout</a></td>
	</tr>
<?php
	} ?>
</table>

<div class="clrb"><br /></div>

<?php
	if($security->is_logged_in() && $security->is_admin()){ ?>

<table cellspacing="0" cellpadding="0" class="menu">
	<tr class="title"><th>Admin</th></tr>
	<tr class="middle<?php echo is_page('admin_logs.php'); ?>">
		<td><a href="admin_logs.php">Admin Logs</a></td>
	</tr>
	<tr class="middle<?php echo is_page('admin_users.php'); ?> bottom">
		<td><a href="admin_users.php">User List</a></td>
	</tr>
</table>

<?php
	} ?>

<div class="clrb"><br /></div>

</div>

<div class="fright" style="width: 500px">
