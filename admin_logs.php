<?php
include_once('header.php');
include_once('classes/log.php');

$security->require_login();

date_default_timezone_set(Config::SYS_TIMEZONE);

$log_obj = new Log();
$logs = $log_obj->get_all_logs();

function wrap_empty($string){
	if(empty($string) || $string == 'failed match')
		return '(none)';
	else
		return str_replace(' ', '&nbsp;', $string);
}

?>

<h2>Admin Logs</h2>

<table cellspacing="0" cellpadding="4" class="log">

<tr>
	<th>Information</th>
	<th style="padding-left: 30px">Message Data</th>
</tr>

<?php foreach($logs as $log){ ?>
<tr class="<?php echo($log->success?'success':'failed'); ?>">
	<td>
		<b><?php echo wrap_empty($log->character); ?></b>
		<br />
		<?php echo ucwords(wrap_empty($log->universe)); ?>
		<br />
		<?php echo date('Y-m-d', $log->time); ?>
		<br />
		<?php echo date('H:i:s', $log->time); ?>
<?php
		if($security->acct->privileges == 1){ ?>
		<br />
		<?php echo $log->ip_address; ?>
<?php
		} ?>
	</td>
	<td style="padding-left: 30px">
		<b><?php echo $log->subject; ?></b>
		<br />
		<?php echo $log->body; ?>
	</td>
</tr>
<?php } ?>

</table>

<?php include_once('footer.php'); ?>
