<?php include_once('header.php'); ?>

<h2>So what is it?</h2>

Once upon a time there was a script that could detect combats and send SMS messages when it occurred.  This system used Twitter, since it had a simple interface to send SMS messages when you sent/received a tweet.  In August 2008, this functionality was disabled.  Since then, nobody has come forward to provide such features again, until now.

<h2>How does it work?</h2>

Like the other script, this project runs in your browser and detects when a combat log is generated.  When this occurs, it sends a message to this server (or any server, as this software is open source), and this server sends out a message via SMS gateways set up by your cell phone carrier.  Because this is open source, and SMS gateways are fairly common, this should hopefully be a much more long term solution than using Twitter.

<br /><br />

For more information about SMS gateways you can check out <a href="http://en.wikipedia.org/wiki/SMS_gateway">SMS gateway</a> and for a listing of all known gateways visit <a href="http://en.wikipedia.org/wiki/List_of_SMS_gateways">List of SMS gateways</a>.

<h2>So what is it?</h2>

<a href="http://www.youtube.com/watch?v=TxWN8AhNER0">Only joking.</a>

<h2>How to Install</h2>

Register for an account under User, and login to your new account.  You must be using Firefox or Chrome for this to work.  If you are running Firefox, you must install GreaseMonkey.  If you are using Chrome, skip this step.

<br /><br />

<a href="https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/">https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/</a>

<br /><br />

Internet Explorer, Safari, and Opera are currently unsupported.

<br /><br />

Install the script from the menu when you are logged in, and you're done.

<h2>How to Use</h2>

When you wish to activate this SMS functionality, leave your browser open to your NAV screen on Pardus.  When a combat log occurs, the script does the rest.

<h2></h2>

<?php include_once('footer.php'); ?>
