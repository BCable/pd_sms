<?php
$raw_setup = true;
include_once('setup.php');
include_once('classes/account.php');
include_once('classes/message.php');
include_once('classes/log.php');

if(
	!isset($_GET['character']) ||
	!isset($_GET['auth']) ||
	!isset($_GET['amount'])
) js_die('Invalid Input');

$account_obj = new Account();
$acct = $account_obj->get_by_char_auth($_GET['character'], $_GET['auth']);

if($acct->msg_box){
	$dier = js_die;
} else {
	$dier = function($ignore){};
}

if(!$acct->stored){
	$acct = $account_obj->get_by_any_char($_GET['character']);
	$log_obj = new Log();
	$log_obj->generate(
		$acct, $_GET['character'],
		"Pardus Combat Log ({$_GET['amount']}): {$_GET['character']}",
		'<br /><b>FAILED CREDENTIALS</b><br /><br />'.
		"Your character {$_GET['character']} in universe ".
			ucwords($acct->get_universe($_GET['character'])).
			' has triggered a combat log, but had invalid credentials.', false
	);
	$dier('Invalid Credentials');
}

if(isset($_GET['type_msg'])){
	if($acct->sms_on_msg){

		$msg = new Message();
		$msg->send(
			$acct, $_GET['character'],
			"Pardus Message Log ({$_GET['amount']}): {$_GET['character']}",
			"Your character {$_GET['character']} in universe ".
				ucwords($acct->get_universe($_GET['character'])).
				' has received a message.'
		);

		$dier('SMS Sent');
	}

} else {
	$msg = new Message();
	$msg->send(
		$acct, $_GET['character'],
		"Pardus Combat Log ({$_GET['amount']}): {$_GET['character']}",
		"Your character {$_GET['character']} in universe ".
			ucwords($acct->get_universe($_GET['character'])).
			' has triggered a combat log.'
	);

	$dier('SMS Sent');
}

?>
