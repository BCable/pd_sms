<?php
include_once('header.php');
include_once('classes/carrier.php');
include_once('classes/message.php');

$security->require_login();

$error = '';
if(isset($_POST['update'])){

	include_once('classes/validation.php');

	if($_POST['carrier_id'] != -2){
		$carrier_obj = new Carrier();
		$carrier = $carrier_obj->get_by_id($_POST['carrier_id']);
	}

	$_POST['char_orion'] = trim($_POST['char_orion']);
	$_POST['char_artemis'] = trim($_POST['char_artemis']);
	$_POST['char_pegasus'] = trim($_POST['char_pegasus']);

	if(!Validation::is_pardus_char($_POST['char_orion']))
		$_POST['char_orion'] = '';
	if(!Validation::is_pardus_char($_POST['char_artemis']))
		$_POST['char_artemis'] = '';
	if(!Validation::is_pardus_char($_POST['char_pegasus']))
		$_POST['char_pegasus'] = '';

	if(
		empty($_POST['char_orion']) &&
		empty($_POST['char_artemis']) &&
		empty($_POST['char_pegasus'])
	)
		$error = 'Please enter at least one valid character name.';
	elseif(
		!empty($_POST['password']) &&
		$_POST['password'] != $_POST['password_confirm']
	)
		$error = 'Your passwords do not match.';
	elseif(!Validation::is_phone($_POST['phone']))
		$error = 'Your phone number is not in the right format.';
	elseif(
		($_POST['carrier_id'] != -2 && !$carrier->stored) ||
		($_POST['carrier_id'] == -2 && empty($_POST['carrier_custom']))
	)
		$error = 'Please select a carrier.';
	elseif(
		$_POST['carrier_id'] == -2 &&
		!Validation::is_email($_POST['carrier_custom'])
	)
		$error = 'Your custom carrier is not a valid email address.';
	else {
		include_once('classes/account.php');
		$account_obj = new Account();

		if($_POST['char_orion'] != $security->acct->char_orion){
			$char = $account_obj->get_by_char('orion', $_POST['char_orion']);
			if($char->stored)
				$error = true;
		}

		if($_POST['char_artemis'] != $security->acct->char_artemis){
			$char =
				$account_obj->get_by_char('artemis', $_POST['char_artemis']);
			if($char->stored)
				$error = true;
		}

		if($_POST['char_pegasus'] != $security->acct->char_pegasus){
			$char =
				$account_obj->get_by_char('pegasus', $_POST['char_pegasus']);
			if($char->stored)
				$error = true;
		}

		if($error === true)
			$error =
				'Character name already in use.  If you are certain all ' .
				'characters are typed in correctly, please contact me to fix ' .
				'this problem immediately.';
	}

	if(empty($error)){
		include_once('classes/account.php');
		$account_obj = new Account();
		$account_obj->update(
			$security->acct->id,
			$_POST['char_orion'], $_POST['char_artemis'],
			$_POST['char_pegasus'],
			Validation::get_phone($_POST['phone']),
			$_POST['carrier_id'], $_POST['carrier_custom'],
			!empty($_POST['msg_box']), !empty($_POST['sms_on_msg'])
		);
		if(!empty($_POST['password'])){
			$account_obj->update_password(
				$security->acct->id, $_POST['password']
			);
			$security->refresh();
			die_clean('Succesfully updated information (including password).');
		} else {
			$security->refresh();
			die_clean('Succesfully updated information.');
		}
	}

}

function get_current_var($var){
	global $security;
	if(!empty($_POST[$var]))
		return $_POST[$var];
	else
		return $security->acct->$var;
}

?>

<h2>Edit Information</h2>

Note: You need at least one character filled out, but you can have up to your maximum of three.  This is not required.  The reason all three characters are on the same account is for convenience, so you only have to install the user script once per account.

<br /><br />

If you use someone else's character name, you will be banned.  You actually gain nothing by doing this, as your script will not function correctly when this occurs.

<br /><br />

<?php if(!empty($error)){ ?>
<div id="error"><?php echo $error; ?></div>
<br />
<?php } ?>

<form method="post">
<table cellspacing="0" cellpadding="5" class="input_form">
<tr>
	<th>Orion&nbsp;Character:</th>
	<td>
		<input type="text" name="char_orion"
		 value="<?php echo get_current_var('char_orion'); ?>" />
	</td>
</tr>
<tr>
	<th>Artemis&nbsp;Character:</th>
	<td>
		<input type="text" name="char_artemis"
		 value="<?php echo get_current_var('char_artemis'); ?>" />
	</td>
</tr>
<tr>
	<th>Pegasus&nbsp;Character:</th>
	<td>
		<input type="text" name="char_pegasus"
		 value="<?php echo get_current_var('char_pegasus'); ?>" />
	</td>
</tr>
<tr>
	<th>
		Password:
		<br />
		<font style="
			font-weight: none !important;
			font-size: 7pt;
		">
			(leave&nbsp;blank&nbsp;to&nbsp;not&nbsp;change)
		</font>
	</th>
	<td><input type="password" name="password" /></td>
</tr>
<tr>
	<th>Password&nbsp;Confirm:</th>
	<td><input type="password" name="password_confirm" /></td>
</tr>
<tr>
	<th>
		Phone&nbsp;Number:
		<br />
		<font style="
			font-weight: none !important;
			font-size: 7pt;
		">
			(11-digit,&nbsp;any&nbsp;punctuation)
		</font>
	</th>
	<td>
		<input type="text" name="phone"
		 value="<?php echo Message::phone_expand(get_current_var('phone')); ?>" />
	</td>
</tr>
<tr>
	<th>
		Phone&nbsp;Carrier:
		<br />
		<font style="
			font-weight: none !important;
			font-size: 7pt;
		">
			(if&nbsp;your&nbsp;carrier&nbsp;is&nbsp;not&nbsp;here,&nbsp;contact&nbsp;me)
			<br />
			* untested carrier; may or may not work; please tell me your results
			<br />
			<a href="carrier_info.php">** more information is available about this carrier</a>
		</font>
	</th>
	<td>
		<select name="carrier_id">
			<option value="-1">-- SELECT A CARRIER --</option>
			<option value="-2" style="font-style: italic"
				<?php echo(-2==get_current_var('carrier_id')?' selected="selected"':null); ?>
			>
				Custom Carrier
			</option>
<?php
			$carrier_obj = new Carrier();
			$carriers = $carrier_obj->get_all_carriers();
			$last_region = '';
			foreach($carriers as $carrier){
				if($last_region != $carrier->region){
					$last_region = $carrier->region; ?>
					<option disabled="disabled"></option>
					<option disabled="disabled"
					>
						- <?php echo strtoupper($carrier->region); ?> -
					</option>
<?php
				} ?>
				<option value="<?php echo $carrier->id; ?>"
				<?php echo($carrier->id==get_current_var('carrier_id')?' selected="selected"':null); ?>
				>
					<?php echo $carrier->name; ?>
				</option>
<?php
			} ?>
		</select>
	</td>
</tr>
<tr>
	<th>
		Custom&nbsp;Carrier&nbsp;(optional):
		<br />
		<font style="
			font-weight: none !important;
			font-size: 7pt;
		">
			<a href="carriers.php">(format&nbsp;and&nbsp;example&nbsp;info)</a>
			<br />
			(also make sure to select "Custom&nbsp;Carrier" above)
		</font>
	</th>
	<td>
		<input type="text" name="carrier_custom"
		 value="<?php echo get_current_var('carrier_custom'); ?>" />
	</td>
</tr>
<tr>
	<th>
		Display&nbsp;Message&nbsp;Box:
	</th>
	<td>
		<input type="checkbox" name="msg_box"
		 value="1"<?php echo (get_current_var('msg_box')?' checked="checked"':''); ?> />
	</td>
</tr>
<tr>
	<th>
		SMS&nbsp;on&nbsp;Pardus&nbsp;Message:
	</th>
	<td>
		<input type="checkbox" name="sms_on_msg"
		 value="1"<?php echo (get_current_var('sms_on_msg')?' checked="checked"':''); ?> />
	</td>
</tr>
<tr>
	<td colspan="2">
		<input type="submit" name="update" value="Update Info" />
	</td>
</tr>
</table>
</form>

<br />

<?php include_once('footer.php'); ?>
