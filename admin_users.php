<?php
include_once('header.php');
include_once('classes/account.php');
include_once('classes/carrier.php');
include_once('classes/message.php');

$security->require_login();

$account_obj = new Account();
$accounts = $account_obj->get_all_accounts();

function wrap_empty($string){
	if(empty($string) || $string == 'failed match')
		return '(none)';
	else
		return str_replace(' ', '&nbsp;', $string);
}

?>

<h2>User List</h2>

<table cellspacing="0" cellpadding="4" class="log">

<tr>
	<th>Usernames</th>
	<th style="padding-left: 30px">Carrier Info</th>
</tr>

<?php foreach($accounts as $acct){ ?>
<tr>
	<td>
		<?php echo wrap_empty($acct->char_orion); ?>
		<br />
		<?php echo wrap_empty($acct->char_artemis); ?>
		<br />
		<?php echo wrap_empty($acct->char_pegasus); ?>
	</td>
	<td style="padding-left: 30px">
		<?php echo Message::phone_expand($acct->phone); ?>
		<br />
		<?php
			if($acct->carrier_id == -2)
				echo "Custom Carrier<br />{$acct->carrier_custom}";
			else {
				$carrier_obj = new Carrier();
				$carrier = $carrier_obj->get_by_id($acct->carrier_id);
				echo "{$carrier->region} - {$carrier->name}";
			}
		?>
	</td>
</tr>
<?php } ?>

</table>

<?php include_once('footer.php'); ?>
