<?php
include_once('classes/generic.php');

class Carrier extends Generic {

	var $stored = false;

	var $id;
	var $name;
	var $region;
	var $format;

	function Carrier(){}

	function class_store($ret){
		$this->id = $ret['id'];
		$this->name = $ret['name'];
		$this->region = $ret['region'];
		$this->format = $ret['format'];
		if(intval($this->id)>0)
			$this->stored = true;
		return $this;
	}

	function get_by_id($id){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM carrier
			WHERE id = %d
			LIMIT 1', $id
		));
		return $this->get_object($ret);
	}

	function get_all_carriers(){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM carrier
			ORDER BY region, name
		'));
		return $this->get_objects($ret);
	}

}

?>
