<?php
include_once('classes/account.php');

class Security {

	function Security(){}

	var $acct = null;

	function start(){
		session_start();
		if($this->is_logged_in()){
			$this->acct = unserialize($_SESSION['account']);
		}
	}

	function is_logged_in(){
		if(isset($_SESSION['account']))
			return true;
		else
			return false;
	}

	function is_admin(){
		if($this->is_logged_in()){
			if($this->acct->privileges == 1)
				return true;
			else
				return false;
		} else
			return false;
	}

	function require_login(){
		if(!$this->is_logged_in()){
			error_clean('Not logged in.');
		}
	}

	function require_admin(){
		if(!$this->is_admin()){
			error_clean('Not enough permissions.');
		}
	}

	function register($acct){
		$_SESSION['account'] = serialize($acct);
		$this->acct = $_SESSION['account'];
	}

	function refresh(){
		$account_obj = new Account();
		$this->register($account_obj->get_by_id($this->acct->id));
	}

}

?>
