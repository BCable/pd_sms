<?php
include_once('classes/config.php');
include_once('classes/carrier.php');
include_once('classes/log.php');

class Message {

	function Message(){}

	function phone_expand($phone){
		while(strlen($phone)<11){
			$phone = "0{$phone}";
		}
		return $phone;
	}

	function send($acct, $character, $subject, $body){
		$carrier_obj = new Carrier();

		if($acct->carrier_id == -2){
			$to = $acct->carrier_custom;
		} else {
			$carrier = $carrier_obj->get_by_id($acct->carrier_id);
			if($carrier->stored)
				$to = $carrier->format;
		}

		// parse 'to' address
		if(isset($to)){
			// 11
			$cur_phone = $this->phone_expand($acct->phone);
			$to = str_replace('[11]', $cur_phone, $to);

			// 5-10
			for($i=10; $i>=5; $i--){
				$cur_phone = substr($cur_phone, 1);
				$to = str_replace("[{$i}]", $cur_phone, $to);
			}

			$ret = mail(
				$to, $subject, $body,
				'From: '.Config::SMS_FROM_EMAIL."\n"
			);

		} else $ret = false;

		$log = new Log();
		$log->generate($acct, $character, $subject, $body, $ret);
	}

}

?>
