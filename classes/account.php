<?php
include_once('classes/generic.php');

class Account extends Generic {

	var $stored = false;

	var $id;
	var $char_orion;
	var $char_artemis;
	var $char_pegasus;
	var $password;
	var $privileges;
	var $phone;
	var $carrier_id;
	var $carrier_custom;
	var $msg_box;
	var $sms_on_msg;

	function Account(){}

	function class_store($ret){
		$this->id = $ret['id'];
		$this->char_orion = $ret['char_orion'];
		$this->char_artemis = $ret['char_artemis'];
		$this->char_pegasus = $ret['char_pegasus'];
		$this->password = $ret['password'];
		$this->privileges = $ret['privileges'];
		$this->phone = $ret['phone'];
		$this->carrier_id = $ret['carrier_id'];
		$this->carrier_custom = $ret['carrier_custom'];
		$this->msg_box = $ret['msg_box'];
		$this->sms_on_msg = $ret['sms_on_msg'];
		if(intval($this->id)>0)
			$this->stored = true;
		return $this;
	}

	function insert(
		$char_orion, $char_artemis, $char_pegasus,
		$password, $phone, $carrier_id, $carrier_custom
	){
		global $sql;
		$sql->query_to(sprintf('
			INSERT INTO account (
				char_orion, char_artemis, char_pegasus,
				password, privileges, phone, carrier_id, carrier_custom,
				msg_box, sms_on_msg
			) VALUES (
				"%s", "%s", "%s", "%s", %d, %d, %d, "%s", %d, %d
			)',
			mysql_real_escape_string($char_orion),
			mysql_real_escape_string($char_artemis),
			mysql_real_escape_string($char_pegasus),
			sha1($password), 0, $phone, $carrier_id,
			mysql_real_escape_string($carrier_custom),
			1, 0
		));

		return $sql->success();
	}

	function update(
		$id, $char_orion, $char_artemis, $char_pegasus,
		$phone, $carrier_id, $carrier_custom,
		$msg_box, $sms_on_msg
	){
		global $sql;
		$sql->query_to(sprintf('
			UPDATE account SET
				char_orion = "%s",
				char_artemis = "%s",
				char_pegasus = "%s",
				phone = %d,
				carrier_id = %d,
				carrier_custom = "%s",
				msg_box = %d,
				sms_on_msg = %d
			WHERE id = %d
			LIMIT 1',
			mysql_real_escape_string($char_orion),
			mysql_real_escape_string($char_artemis),
			mysql_real_escape_string($char_pegasus),
			$phone, $carrier_id,
			mysql_real_escape_string($carrier_custom),
			intval(!empty($msg_box)), intval(!empty($sms_on_msg)),
			intval($id)
		));

		return $sql->success();
	}

	function update_password($id, $password){
		global $sql;
		$sql->query_to(sprintf('
			UPDATE account SET
				password = "%s"
			WHERE id = %d
			LIMIT 1', sha1($password), $id
		));

		return $sql->success();
	}

	function get_by_char($universe, $character){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM account
			WHERE char_%s = "%s"
			LIMIT 1', $universe, mysql_real_escape_string($character)
		));
		return $this->get_object($ret);
	}

	function get_by_any_char($character){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM account
			WHERE
				char_orion = "%s" OR
				char_artemis = "%s" OR
				char_pegasus = "%s"
			LIMIT 1',
			mysql_real_escape_string($character),
			mysql_real_escape_string($character),
			mysql_real_escape_string($character)
		));
		return $this->get_object($ret);
	}

	function get_by_char_auth($character, $auth){
		// this one takes in a sha1, not a raw password
		global $sql;
		$character = mysql_real_escape_string($character);
		$ret = $sql->query_one(sprintf('
			SELECT * FROM account
			WHERE (
				char_orion = "%s" OR
				char_artemis = "%s" OR
				char_pegasus = "%s"
			) AND password = "%s"
			LIMIT 1', $character, $character, $character, $auth
		));
		return $this->get_object($ret);
	}

	function get_by_id($id){
		global $sql;
		$ret = $sql->query_one(sprintf('
			SELECT * FROM account
			WHERE id = %d
			LIMIT 1', $id
		));
		return $this->get_object($ret);
	}

	function verify_login($character, $password){
		global $sql;
		$character = mysql_real_escape_string($character);
		$ret = $sql->query_one(sprintf('
			SELECT * FROM account
			WHERE
				(
					char_orion = "%s" OR
					char_artemis = "%s" OR
					char_pegasus = "%s"
				) AND password = "%s"
			LIMIT 1', $character, $character, $character, sha1($password)
		));
		return $this->get_object($ret);
	}

	function get_universe($character){
		if($this->char_orion == $character)
			$universe = 'orion';
		elseif($this->char_artemis == $character)
			$universe = 'artemis';
		elseif($this->char_pegasus == $character)
			$universe = 'pegasus';
		else
			$universe = 'failed match';
		return $universe;
	}

	function get_all_accounts(){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM account
			ORDER BY privileges DESC, id ASC
		'));
		return $this->get_objects($ret);
	}

}

?>
