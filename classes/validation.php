<?php

class Validation {

	function is_pardus_char($text){
		if(
			empty($text) || strlen($text)<3 || strlen($text)>20 ||
			preg_replace('/[^a-zA-Z0-9 ]/', '', $text) != $text
		) return false;
		else return true;
	}

	function get_phone($phone){
		return preg_replace('/[^0-9]/','',$phone);
	}

	function is_phone($phone){
		return strlen(Validation::get_phone($phone)) == 11;
	}

	function is_email($email){
		// this allows brackets ([]) in addition to normal email addresses due
		// to the parsing syntax
		$allstuff = '[a-zA-Z0-9!#$%&\'*+\/=?^_`{|}~\-\.\[\]]';
		return preg_match("/^{$allstuff}+@{$allstuff}+$/", $email);
	}

}

?>
