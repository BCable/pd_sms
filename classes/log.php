<?php
include_once('classes/generic.php');

class Log extends Generic {

	var $stored = false;

	var $id;
	var $time;
	var $account_id;
	var $universe;
	var $character;
	var $subject;
	var $body;
	var $success;
	var $ip_address;

	function Log(){}

	function class_store($ret){
		$this->id = $ret['id'];
		$this->time = $ret['time'];
		$this->account_id = $ret['account_id'];
		$this->universe = $ret['universe'];
		$this->character = $ret['character'];
		$this->subject = $ret['subject'];
		$this->body = $ret['body'];
		$this->success = $ret['success'];
		$this->ip_address = $ret['ip_address'];
		$this->stored = true;
		return $this;
	}

	function generate($acct, $character, $subject, $body, $success){
		global $sql;

		$time = time();

		if($acct->stored){
			$id = $acct->id;

			if(empty($character))
				$universe = 'failed match';
			if($acct->char_orion == $character)
				$universe = 'orion';
			elseif($acct->char_artemis == $character)
				$universe = 'artemis';
			elseif($acct->char_pegasus == $character)
				$universe = 'pegasus';
			else
				$universe = 'failed match';

		} else {
			$id = -1;
			$universe = 'failed credentials';
		}

		$sql->query_to(sprintf('
			INSERT INTO log (
				time, account_id, universe, `character`,
				subject, body, success, ip_address
			) VALUES (
				%d, %d, "%s", "%s", "%s", "%s", %d, "%s"
			)',
			$time, $id,
			mysql_real_escape_string($universe),
			mysql_real_escape_string($character),
			mysql_real_escape_string($subject),
			mysql_real_escape_string($body),
			$success, $_SERVER['REMOTE_ADDR']
		));

		return $sql->success();
	}

	function get_user_logs($acct){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM log
			WHERE account_id = %d
			ORDER BY time DESC
			LIMIT %d
			', $acct->id, Config::LOG_MAX
		));
		return $this->get_objects($ret);
	}

	function get_all_logs(){
		global $sql;
		$ret = $sql->query(sprintf('
			SELECT * FROM log
			ORDER BY time DESC
			LIMIT %d
			', Config::LOG_MAX_ADMIN
		));
		return $this->get_objects($ret);
	}

}

?>
