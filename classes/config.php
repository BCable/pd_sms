<?php

class Config {

	// admin info
	const ADMIN_ORION = 'Jetix';
	const ADMIN_ARTEMIS = 'Aeiri';
	const ADMIN_PEGASUS = 'Likor';
	const ADMIN_EMAIL = 'brad@bcable.net';

	// SMS stuff
	const SMS_FROM_EMAIL = 'brad@bcable.net';

	// system settings
	const SYS_TIMEZONE = 'America/Chicago';

	// log config
	const LOG_MAX = 30;
	const LOG_MAX_ADMIN = 50;

	// script config
	const SCRIPT_MESSAGE_GATEWAY = 'http://example.com/pd_sms/gateway.php';

	// database config
	const DB_HOST = 'localhost';
	const DB_USER = 'username';
	const DB_PASS = 'password';
	const DB_NAME = 'database';

}

?>
