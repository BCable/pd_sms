<?php

include_once('classes/config.php');
include_once('classes/quicksql.php');

$sql = new QuickSQL();
$sql->connect(Config::DB_HOST, Config::DB_USER, Config::DB_PASS);
$sql->select_db(Config::DB_NAME);

if(!isset($raw_setup)){
	include_once('classes/account.php');
	include_once('classes/security.php');
	$security = new Security();
	$security->start();

	function die_clean($string){
		echo "<br />\n{$string}";
		include_once('footer.php');
		die();
	}

	function error_clean($string){
		echo "<br />\n<div id=\"error\">Error: {$string}</div>";
		include_once('footer.php');
		die();
	}

} else {
	function js_die($string){
		die('alert("'.addslashes($string).'");');
	}
}

?>
