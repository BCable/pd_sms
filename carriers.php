<?php
include_once('header.php');
include_once('classes/carrier.php');

?>

<h2>Carrier Info</h2>

This is a list of all supported carriers.  Also listed is the format assumed to be their Email-to-SMS gateway.  If any of these are incorrect, please inform me.  I have been upkeeping the Wikipedia's page "<a href="http://en.wikipedia.org/wiki/List_of_SMS_gateways">List of SMS gateways</a>" whenever I change these carriers, so if you spot anything on there that I missed, let me know.

<h2>Format</h2>

The format of these are simple email addresses with special tags that are replaced by the given phone number.  You can manually enter your own into "Custom Carrier", or even your email address if you would rather use that.  For instance, "[11]" represents the whole 11-digit phone number, and "[10]" represents the 10-digit phone number starting from the right.  So the 11-digit phone number "13093196248" gets represented as "3093196248" when "[10]" is used, and "3196248" when "[7]" is used.  You may use numbers between 5 and 11 in brackets like this.

<h2>List of Carriers</h2>

<font style="
	font-weight: none !important;
	font-size: 7pt;
">
* untested carrier; may or may not work; please tell me your results
<br />
<a href="carrier_info.php">** more information is available about this carrier</a>
</font>

<br /><br />

<table cellspacing="0" cellpadding="0" class="carriers">

<?php
	$carrier_obj = new Carrier();
	$carriers = $carrier_obj->get_all_carriers();
	$last_region = '';
	foreach($carriers as $carrier){
		if($last_region != $carrier->region){
			$last_region = $carrier->region; ?>
<tr>
	<th colspan="2"><?php echo $carrier->region; ?></th>
</tr>
<?php
		} ?>
<tr>
	<td><?php echo $carrier->name; ?></td>
	<td><?php echo $carrier->format; ?></td>
</tr>
<?php
	} ?>

</table>

<?php include_once('footer.php'); ?>
