</div>

<div style="
	float: left;
	clear: both;
	width: 100%;
	text-align: center;
	border-top: 2px solid #A1A1AF;
	padding-top: 10px;
	margin-top: 10px;
">
	<?php
	$admin = array();
	if(Config::ADMIN_ORION != ''){
		$admin[] = Config::ADMIN_ORION . ' (Orion)';
	}
	if(Config::ADMIN_ORION != ''){
		$admin[] = Config::ADMIN_ARTEMIS . ' (Artemis)';
	}
	if(Config::ADMIN_ORION != ''){
		$admin[] = Config::ADMIN_PEGASUS . ' (Pegasus)';
	}
	if(Config::ADMIN_EMAIL != ''){
		$admin[] =
			'<a href="mailto:' . Config::ADMIN_EMAIL . '">' .
			Config::ADMIN_EMAIL . '</a> (Email)';
	}
	echo implode('&nbsp;<b>&middot;</b>&nbsp;',$admin);
	?>
	<br /><br />
</div>

</div>

</body>

</html>
