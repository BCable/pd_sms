<?php
include_once('header.php');
include_once('classes/account.php');

if(isset($_POST['login'])){
	$account_obj = new Account();
	$acct = $account_obj->verify_login($_POST['character'], $_POST['password']);
	if($acct->stored){
		$security->register($acct);
		header('Location: index.php');
		die();
	}
	$error = 'Login failed.';
}

?>

<h2>Login</h2>

You can use any of your character names to login to this system.

<br /><br />

<?php if(!empty($error)){ ?>
<div id="error">Error: <?php echo $error; ?></div>
<br />
<?php } ?>

<form method="post">
<table cellspacing="0" cellpadding="0" class="input_form">
<tr>
	<th>Character:</th>
	<td>
		<input type="text" name="character"
		 value="<?php echo $_POST['character']; ?>" />
	</td>
</tr>
<tr>
	<th>Password:</th>
	<td><input type="password" name="password" /></td>
</tr>
<tr>
	<td colspan="2"><input type="submit" name="login" value="Login" /></td>
</tr>
</table>
</form>

<br /><br />

<?php include_once('footer.php'); ?>
