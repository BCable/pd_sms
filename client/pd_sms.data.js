// ==UserScript==
// @name        Pardus Combat SMS Script
// @namespace   http://plimini.net/pd_sms/
// @version     1.0.0
// @description Triggers an SMS when a new combat log occurs.
// @include     http://*.pardus.at/msgframe.php
// @copyright   2011, Brad Cable
// @license     MIT
// ==/UserScript==

// config
var link = "<LINK>";
var auth = "<AUTH>";

// create trim() method
function trim(str){
	return str.toString().replace(/^\s+|\s+$/g, "");
}

function cookie_yank(cookname){
	var cookreg = new RegExp(".*" + cookname + "=([^;]*);.*");
	if(!document.cookie.match(cookreg)){
		// last cookie set?
		var cookreg = new RegExp(".*" + cookname + "=([^;]*)$");
		if(document.cookie.match(cookreg)){
			var cookie_val = document.cookie.replace(cookreg, "$1");
			return trim(cookie_val);
		}
		else return false;
	}
	var cookie_val = document.cookie.replace(cookreg, "$1");
	return trim(cookie_val);
}
function cookie_push(cookname, cookval, time){
	var multiplier = 1;
	var expires = new Date();
	// it seems that at least some implementations use microseconds, not
	// seconds...
	if(expires.getTime().toString().length == 13)
		multiplier = 1000;

	expires.setTime(expires.getTime()+1000*time);
	document.cookie =
		trim(cookname) + "=" + trim(cookval) +
		"; expires=" + expires.toGMTString() + ";";
}

// get universe
var infoImg = document.getElementsByTagName("img")[0];
var universe =
	infoImg.alt.replace(/^(Artemis|Orion|Pegasus): .*/, "$1");

universe = universe.toLowerCase();

// get amount for combats
var cmbt_item = document.getElementById("new_cl");
if(cmbt_item != null){
	var amount = parseInt(document.getElementById('new_cl').innerHTML);

	// check cookie
	if(cookie_yank("pd_"+universe+"_sms_amount") != amount){
		// get universe name and username
		var character = infoImg.alt.replace(/^(Artemis|Orion|Pegasus): /, "");
		character = character.replace(" ", "%20");

		// trigger message
		var trigger = document.createElement("script");
		trigger.src =
			link +
			"?character=" + character +
			"&auth=" + auth +
			"&amount=" + amount;
		document.body.appendChild(trigger);

		// set cookie for one day
		cookie_push("pd_"+universe+"_sms_amount", amount, 86400);
	}

// reset to zero
} else {
	cookie_push("pd_"+universe+"_sms_amount", 0, 86400);
}

// get amount for messages
var msg_item = document.getElementById("new_msg");
if(msg_item != null){
	var amount = parseInt(document.getElementById('new_msg').innerHTML);

	// check cookie
	if(cookie_yank("pd_"+universe+"_sms_msg_amount") != amount){
		// get universe name and username
		var character = infoImg.alt.replace(/^(Artemis|Orion|Pegasus): /, "");
		character = character.replace(" ", "%20");

		// trigger message
		var trigger = document.createElement("script");
		trigger.src =
			link +
			"?character=" + character +
			"&auth=" + auth +
			"&amount=" + amount +
			"&type_msg=1";
		document.body.appendChild(trigger);

		// set cookie for one day
		cookie_push("pd_"+universe+"_sms_msg_amount", amount, 86400);
	}

// reset to zero
} else {
	cookie_push("pd_"+universe+"_sms_msg_amount", 0, 86400);
}
