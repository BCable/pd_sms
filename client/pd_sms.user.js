<?php
ini_set('include_path', ini_get('include_path').':..');
include_once('setup.php');

$security->require_login();

header('Pragma: public');
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Content-Description: File Transfer');
header('Content-Type: text/javascript');
$contents = file_get_contents('pd_sms.data.js');
$contents = str_replace('<LINK>', Config::SCRIPT_MESSAGE_GATEWAY, $contents);
$contents = str_replace('<AUTH>', $security->acct->password, $contents);
echo $contents;

?>
