<?php include('header.php'); ?>

<h2>Extra Carrier Info</h2>

I will keep this up to date with any additional information I find out about carriers until they are supported.

<h3>United Kingdom</h3>

<h4>O2 UK</h4>

I believe this service to be opt-in, and I need someone to attempt calling them and asking about this service.  So far I have found little information about it.  Just ask about their Email-to-SMS Gateway service.

<h4>Vodafone</h4>

This service might be opt-in or it might be disabled.  I have seen references to this <b>NOT BEING FREE</b> and costing 10p per message.  I do need someone to attempt calling them and asking about this service, and filling me in with the details.  Just ask about their Email-to-SMS Gateway service.

<?php include('footer.php'); ?>
