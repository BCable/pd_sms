-- phpMyAdmin SQL Dump
-- version 3.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 26, 2011 at 09:35 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pardus_sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `account`
--

CREATE TABLE IF NOT EXISTS `account` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `char_artemis` text,
  `char_orion` text,
  `char_pegasus` text,
  `password` text,
  `privileges` smallint(2) DEFAULT NULL,
  `phone` bigint(11) DEFAULT NULL,
  `carrier_id` smallint(5) DEFAULT NULL,
  `carrier_custom` text,
  `msg_box` tinyint(1) DEFAULT NULL,
  `sms_on_msg` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `carrier`
--

CREATE TABLE IF NOT EXISTS `carrier` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` text,
  `region` text,
  `format` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=188 ;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `time` int(12) DEFAULT NULL,
  `account_id` int(8) NOT NULL,
  `universe` text NOT NULL,
  `character` text NOT NULL,
  `subject` text,
  `body` text NOT NULL,
  `success` tinyint(1) DEFAULT NULL,
  `ip_address` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=215 ;
