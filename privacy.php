<?php include_once('header.php'); ?>

<h2>Privacy Policy</h2>

My primary purpose for making this script is to provide a service to the Pardus community.  Privacy is a priority and I promise not to use this information for bad.

<h2>Phone Numbers/Carriers</h2>

I will not divulge your phone number or carrier to anyone, ever.  I will also never call, message, or in any way ever use your phone number unless directly specified by the application (IE: to send notifications when your browser automatically notifies the server).

<h2>Game Related Privacy</h2>

I will not send an SMS that says your ambush was triggered randomly, and I won't share who you are across universes.  I think that covers everything.

<h2>Legal</h2>

Unless I am presented by a warrant or court order in the United States, your information is safe.  I can't imagine a judge saying, "we can't figure out who this guy is but he plays Pardus and uses the Pardus Combat SMS Relay", so I think you are okay.  Unless maybe you kill some famous person and announce it on Pardus or something.

<br /><br />

<?php include_once('footer.php'); ?>
